package com.mumux.framework.ioc.entity;


public class Ioc {

    private Aop aop;

    private String name;

    public Aop getAop() {
        return aop;
    }

    public void setAop(Aop aop) {
        this.aop = aop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void create() {
        System.out.println("Aop ：" + name + "  ,依赖于Ioc: " + aop.getName());
        System.out.println("我是反转控制的小能手");
    }
}
