package com.mumux.framework.ioc.entity;


public class Spring {

    private Aop aop;

    private Ioc ioc;

    public void show(){

        aop.aspectj();
        ioc.create();

    }

    public Aop getAop() {
        return aop;
    }

    public void setAop(Aop aop) {
        this.aop = aop;
    }

    public Ioc getIoc() {
        return ioc;
    }

    public void setIoc(Ioc ioc) {
        this.ioc = ioc;
    }

    @Override
    public String toString() {
        return "Spring{" +
                "aop=" + aop +
                ", ioc=" + ioc +
                '}';
    }
}
