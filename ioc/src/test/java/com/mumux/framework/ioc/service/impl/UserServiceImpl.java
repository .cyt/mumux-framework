package com.mumux.framework.ioc.service.impl;

import com.mumux.framework.ioc.annotation.Autowired;
import com.mumux.framework.ioc.annotation.Service;
import com.mumux.framework.ioc.service.OrderService;
import com.mumux.framework.ioc.service.UserService;


@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private OrderService orderService;

    @Override
    public int add(String a) {
        System.out.println("测试注解创建userService");
        orderService.add();
        return 0;
    }
}
