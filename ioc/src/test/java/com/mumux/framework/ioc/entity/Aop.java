package com.mumux.framework.ioc.entity;


public class Aop {

    private Ioc ioc;

    private String name;

    public Ioc getIoc() {
        return ioc;
    }

    public void setIoc(Ioc ioc) {
        this.ioc = ioc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void aspectj() {
        System.out.println("Ioc ：" + name + " , 依赖于Aop : " + ioc.getName());
        System.out.println("我是一个USB，即插即用");
    }
}
