package com.mumux.framework.ioc;

import com.mumux.framework.ioc.core.AnnotationApplicationContext;
import com.mumux.framework.ioc.core.JsonApplicationContext;
import com.mumux.framework.ioc.core.XmlApplicationContext;
import com.mumux.framework.ioc.entity.Spring;
import com.mumux.framework.ioc.service.UserService;

public class Test {

    public static void main(String[] args) throws Exception {
          //JSON配置方式启用这一行代码
//        JsonApplicationContext applicationContext = new JsonApplicationContext("application.json");
        //XML配置方式启用这一行代码
//        XmlApplicationContext applicationContext = new XmlApplicationContext("application.xml");
//        applicationContext.init();
//        Spring spring = (Spring) applicationContext.getBean("spring");
//
//        spring.show();
        AnnotationApplicationContext annotationApplicationContext = new AnnotationApplicationContext("com.mumux.framework.ioc.service");
        annotationApplicationContext.init();
        UserService userService = (UserService) annotationApplicationContext.getBean("userService");
        userService.add("123");

    }

}
