package com.mumux.framework.ioc.service.impl;

import com.mumux.framework.ioc.annotation.Service;
import com.mumux.framework.ioc.service.OrderService;

@Service("orderService")
public class OrderServiceImpl implements OrderService {
    @Override
    public void add() {
        System.out.println("Autowired已经成功注入");
    }
}
