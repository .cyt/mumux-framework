package com.mumux.framework.ioc.handler;

import java.lang.reflect.Method;

public class AspectProxyHandlerSupport {

    protected Method aspectMethod;

    protected Object aspectObj;

    protected Object beProxyObject;

    public Object getBeProxyObject() {
        return beProxyObject;
    }

}
