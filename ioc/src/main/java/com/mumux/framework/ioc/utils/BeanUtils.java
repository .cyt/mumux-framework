package com.mumux.framework.ioc.utils;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;

import java.lang.reflect.Constructor;

/**
  * @Author:huangjuncong
  * @Description: 负责处理对象的实例化，这里我使用了 cglib 这个工具包
  * @param
  **/
public class BeanUtils {

    public static <T> T instanceByCglib(Class<T> clz,Constructor ctr,Object[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clz);
        enhancer.setCallback(NoOp.INSTANCE);

        if (ctr == null) {
            return (T) enhancer.create();
        } else {
            return (T) enhancer.create(ctr.getParameterTypes(), args);

        }
    }

    public static <T> T instance(Class<T> clz) throws IllegalAccessException, InstantiationException {
        return clz.newInstance();
    }

}
