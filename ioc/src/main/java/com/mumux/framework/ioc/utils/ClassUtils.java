package com.mumux.framework.ioc.utils;

/**
  * @Author:huangjuncong
  * @Description: 负责处理 Java 类的加载
  **/
public class ClassUtils {

    public static ClassLoader getDefultClassLoader(){
        return Thread.currentThread().getContextClassLoader();
    }

    /**
      * @Author:huangjuncong
      * @Description: 通过 className 这个参数获取对象的 Class
      * @param: className
      **/
    public static Class loadClass(String className){
        try {
            return getDefultClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

}
