package com.mumux.framework.ioc.bean;


import java.util.Arrays;
import java.util.List;

/**
  * @Author:huangjuncong
  * @Description: 核心数据结构。用于描述我们需要 IoC 框架管理的对象。
  * 包含了对象的 name，class的名称。如果是接口的实现，还有该对象实现的接口。
  * 以及构造函数的传参的列表 constructorArgs 和需要注入的参数列表 `propertyArgs。
  * @param
  **/
public class BeanDefinition {

    private String name;

    private String className;

    private String[] interfaceName;

    private List<ConstructorArg> constructorArgs;

    private List<PropertyArg> propertyArgs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String[] getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String[] interfaceName) {
        this.interfaceName = interfaceName;
    }

    public List<ConstructorArg> getConstructorArgs() {
        return constructorArgs;
    }

    public void setConstructorArgs(List<ConstructorArg> constructorArgs) {
        this.constructorArgs = constructorArgs;
    }

    public List<PropertyArg> getPropertyArgs() {
        return propertyArgs;
    }

    public void setPropertyArgs(List<PropertyArg> propertyArgs) {
        this.propertyArgs = propertyArgs;
    }

    @Override
    public String toString() {
        return "BeanDefinition{" +
                "name='" + name + '\'' +
                ", className='" + className + '\'' +
                ", interfaceName=" + Arrays.toString(interfaceName) +
                ", constructorArgs=" + constructorArgs +
                ", propertyArgs=" + propertyArgs +
                '}';
    }
}
