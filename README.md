# mumux-framework

#### 介绍
教你轻松手写实现一款入门级的Spring框架，后续会继续完善这个框架，目前处于起步状态。
你将会了解：

什么是依赖注入和控制反转
Ioc有什么用
Spring的 Ioc 是怎么实现的
按照Spring的思路开发一个简单的Ioc框架

Spirng IoC 主要是以下几个步骤。


```
1. 初始化 IoC 容器。
2. 读取配置文件。
3. 将配置文件转换为容器识别对的数据结构（这个数据结构在Spring中叫做 BeanDefinition
4. 利用数据结构依次实例化相应的对象
5. 注入对象之间的依赖关系
```
参考 Spirng 的 IoC 实现，去除所有与核心原理无关的逻辑。极简的实现 IoC 的框架。 项目使用 json 作为配置文件或者你也可以使用XML作为配置文件，这两种配置随意选择。使用 maven 管理 jar 包的依赖。在这个框架中我们的对象都是单例的，并不支持Spirng的多种作用域。框架的实现使用了cglib 和 Java 的反射。后续会添加注解配置，加入mvc等

你将会了解：

Aop是什么？
为什么要使用Aop？
Spirng 实现Aop的思路是什么
自己根据Spring 思想实现一个 Aop框架

Spring 的 Aop 实现主要以下几个步骤：


```
1.初始化 Aop 容器。
2.读取配置文件。
3.将配置文件装换为 Aop 能够识别的数据结构 – Advisor。这里展开讲一讲这个advisor。Advisor对象中包又含了两个重要的数据结构，一个是 Advice，一个是 Pointcut。Advice的作用就是描述一个切面的行为，pointcut描述的是切面的位置。两个数据结的组合就是”在哪里，干什么“。这样 Advisor 就包含了”在哪里干什么“的信息，就能够全面的描述切面了。
4.Spring 将这个 Advisor 转换成自己能够识别的数据结构 – AdvicedSupport。Spirng 动态的将这些方法拦截器织入到对应的方法。
5.生成动态代理代理。
6.提供调用，在使用的时候，调用方调用的就是代理方法。也就是已经织入了增强方法的方法。
```


#### 软件架构

软件架构说明
mumux-framwork主要包括一下三个模块：

aop模块：切面

ioc模块：反转控制，

mergerequest模块：高并发下的请求合并

codeGenerator模块：代码生成器



#### 使用说明
AOP和IOC有两种配置方式，你可以选择JSON配置方式，也可以选择XML配置方式。

使用文档见wiki:[使用文档wiki](https://gitee.com/huangjuncong/mumux-framework/wikis/%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3?sort_id=1332955)


