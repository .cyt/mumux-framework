package com.mumux.framwork.mvc.controller;

import com.mumux.framework.mvc.annotation.Controller;
import com.mumux.framework.mvc.annotation.RequestMapping;

@Controller("/test")
public class TestController {

    @RequestMapping("/hello")
    public String hello(){
        System.out.println("执行自定义Controller");
        return "index";
    }

}
