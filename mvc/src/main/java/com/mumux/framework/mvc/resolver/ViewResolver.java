package com.mumux.framework.mvc.resolver;

import lombok.Data;

@Data
public class ViewResolver {
    private String prefix;
    private String suffix;


    public String jspMapping(String value){
        return this.prefix+value+this.suffix;
    }
}
