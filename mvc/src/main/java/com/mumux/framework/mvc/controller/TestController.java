package com.mumux.framework.mvc.controller;

import com.mumux.framework.mvc.annotation.Controller;
import com.mumux.framework.mvc.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/test")
public class TestController {

    @RequestMapping(value = "/hello")
    public String hello(){
        System.out.println("执行自定义Controller");
        return "index";
    }

}
