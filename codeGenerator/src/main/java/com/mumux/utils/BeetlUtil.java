package com.mumux.utils;

import com.mumux.table.DataBaseFields;
import com.mumux.template.MumuxTemplate;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by cong on 2019/4/21.
 */
public class BeetlUtil {

    public static void main(String[] args) {
        try{
            String dir = System.getProperty("user.dir") + File.separator + "codeGenerator" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "config" + File.separator;
            String fileName = "codeGenerator.properties";
            Properties properties = new Properties();
            properties.load(new FileInputStream(dir + fileName));
            //构建模板对象
            MumuxTemplate mumuxTemplate = new MumuxTemplate();

            /*
            设置公共属性：实体类名，模块包名
             */
            mumuxTemplate.setRootPath(properties.getProperty("RootPath"));
            mumuxTemplate.setEntityClassName(properties.getProperty("EntityClassName"));
            mumuxTemplate.setEntityName(properties.getProperty("EntityName"));
            mumuxTemplate.setPackageName(properties.getProperty("PackageName"));
            mumuxTemplate.setCollectionName(properties.getProperty("CollectionName"));
            mumuxTemplate.setList(getDataBaseFieldList(properties.getProperty("SqlFile")));

            /*
            设置Entity属性，生成Entity
             */
            mumuxTemplate.setTemplateName("Entity.txt");
            CreateUtil.createEntity(mumuxTemplate);

            /*
            生成DAO
             */
            mumuxTemplate.setTemplateName("Mapper.txt");
            CreateUtil.createDaoInterface(mumuxTemplate);

            /*
             生成Service
             */
            mumuxTemplate.setTemplateName("Service.txt");
            CreateUtil.createServiceInterface(mumuxTemplate);

            /*
            设置Controller属性，生成Controller
             */
            mumuxTemplate.setPackageMapPath();
            mumuxTemplate.setTemplateName("Controller.txt");
            CreateUtil.createController(mumuxTemplate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static List<DataBaseFields> getDataBaseFieldList(String fileName) throws Exception{
        List<DataBaseFields> dataBaseFieldsList = new ArrayList<DataBaseFields>();
        ObjectMapper objectMapper = new ObjectMapper();
        List<Map<String,Object>> list = (List<Map<String,Object>>)objectMapper.readValue(new FileInputStream(System.getProperty("user.dir") + File.separator + "codeGenerator" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "sql" + File.separator + fileName), List.class);
        for(Map<String,Object> map : list){
            DataBaseFields dataBaseFields = new DataBaseFields();
            dataBaseFields.setName(map.get("name").toString());
            dataBaseFields.setType(map.get("type").toString());
            dataBaseFields.setNull((Boolean) map.get("isNull"));
            dataBaseFields.setIndex((Boolean)map.get("isIndex"));
            dataBaseFields.setDefaultValue(map.get("defaultValue"));
            if(map.containsKey("autoIncrement")){
                dataBaseFields.setAutoIncrement((Boolean)map.get("autoIncrement"));
            }
            dataBaseFields.setCamelCaseName(map.get("name").toString().substring(0, 1).toUpperCase() + map.get("name").toString().substring(1));
            if(map.get("description") != null)
                dataBaseFields.setDescription(map.get("description").toString());
            dataBaseFieldsList.add(dataBaseFields);
        }
        return dataBaseFieldsList;
    }

}
