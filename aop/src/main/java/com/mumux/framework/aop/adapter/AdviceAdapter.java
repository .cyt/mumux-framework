package com.mumux.framework.aop.adapter;

import com.mumux.framework.aop.advisor.Advisor;
import com.mumux.framework.aop.interceptor.AopMethodInterceptor;

public interface AdviceAdapter {

    AopMethodInterceptor getInterceptor(Advisor advisor);
}
