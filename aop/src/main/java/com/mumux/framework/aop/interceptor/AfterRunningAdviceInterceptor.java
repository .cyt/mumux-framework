package com.mumux.framework.aop.interceptor;

import com.mumux.framework.aop.Invocation.MethodInvocation;
import com.mumux.framework.aop.advisor.AfterRunningAdvice;

/**
  * @Author:huangjuncong
  * @Description: 方法运行结束以后拦截
  **/
public class AfterRunningAdviceInterceptor implements AopMethodInterceptor {

    private AfterRunningAdvice advice;

    public AfterRunningAdviceInterceptor(AfterRunningAdvice advice) {
        this.advice = advice;
    }

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        Object returnVal = mi.proceed();
        advice.after(returnVal,mi.getMethod(),mi.getArguments(),mi);
        return returnVal;
    }
}
