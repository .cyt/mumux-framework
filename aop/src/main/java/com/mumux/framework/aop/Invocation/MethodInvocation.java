package com.mumux.framework.aop.Invocation;

import java.lang.reflect.Method;

/**
  * @Author:huangjuncong
  * @Description: 用于描述方法的调用
  * 描述一个方法的调用包含三个方法，
  * 获取方法本身getMethod,
  * 获取方法的参数getArguments，
  * 还有执行方法本身proceed()。
  **/
public interface MethodInvocation {

    Method getMethod();

    Object[] getArguments();

    Object proceed() throws Throwable;

}
