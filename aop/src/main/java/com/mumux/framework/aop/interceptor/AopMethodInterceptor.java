package com.mumux.framework.aop.interceptor;


import com.mumux.framework.aop.Invocation.MethodInvocation;

/**
  * @Author:huangjuncong
  * @Description: 方法的拦截器, Aop 容器所有拦截器都要实现的接口
  **/
public interface AopMethodInterceptor {

    Object invoke(MethodInvocation mi) throws Throwable;

}
