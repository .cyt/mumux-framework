package com.mumux.framework.aop.advisor;

import com.mumux.framework.aop.interceptor.AopMethodInterceptor;

import java.util.LinkedList;
import java.util.List;

/**
  * @Author:huangjuncong
  * @Description: Spring 将这个 Advisor 转换成自己能够识别的数据结构–AdvicedSupport。
  * Spirng 动态的将这些方法拦截器织入到对应的方法。
  * 这个AdvisedSupport就是 我们Aop框架能够理解的数据结构，这个时候问题就变成了–对于哪个目标，增加哪些拦截器。
  **/
public class AdvisedSupport extends Advisor {

    /**
     * 目标对象
     */
    private TargetSource targetSource;

    /**
     * 拦截器列表
     */
    private List<AopMethodInterceptor> list = new LinkedList<>();

    public void addAopMethodInterceptor(AopMethodInterceptor interceptor){
        list.add(interceptor);
    }

    public void addAopMethodInterceptors(List<AopMethodInterceptor> interceptors){
        list.addAll(interceptors);
    }

    public TargetSource getTargetSource() {
        return targetSource;
    }

    public void setTargetSource(TargetSource targetSource) {
        this.targetSource = targetSource;
    }

    public List<AopMethodInterceptor> getList() {
        return list;
    }

    public void setList(List<AopMethodInterceptor> list) {
        this.list = list;
    }
}
