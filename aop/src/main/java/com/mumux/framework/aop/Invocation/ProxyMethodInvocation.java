package com.mumux.framework.aop.Invocation;


/**
  * @Author:huangjuncong
  * @Description: 代理方法的调用,获取代理的方法
  **/
public interface ProxyMethodInvocation extends MethodInvocation {

    Object getProxy();

}
