package com.mumux.framework.aop.interceptor;

import com.mumux.framework.aop.Invocation.MethodInvocation;
import com.mumux.framework.aop.advisor.BeforeMethodAdvice;

/**
  * @Author:huangjuncong
  * @Description: 方法执行以前拦截
  **/
public class BeforeMethodAdviceInterceptor implements AopMethodInterceptor {

    private BeforeMethodAdvice advice;

    public BeforeMethodAdviceInterceptor(BeforeMethodAdvice advice) {
        this.advice = advice;
    }

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        advice.before(mi.getMethod(),mi.getArguments(),mi);
        return mi.proceed();
    }
}
