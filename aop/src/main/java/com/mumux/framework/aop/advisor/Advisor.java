package com.mumux.framework.aop.advisor;


/**
  * @Author:huangjuncong
  * @Description: Advisor对象中包又含了两个重要的数据结构，一个是 Advice，一个是 Pointcut。
  * Advice的作用就是描述一个切面的行为，pointcut描述的是切面的位置。
  **/
public class Advisor {

    private Advice advice;

    private Pointcut pointcut;

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }

    public Pointcut getPointcut() {
        return pointcut;
    }

    public void setPointcut(Pointcut pointcut) {
        this.pointcut = pointcut;
    }
}
