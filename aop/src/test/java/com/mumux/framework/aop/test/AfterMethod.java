package com.mumux.framework.aop.test;

import com.mumux.framework.aop.advisor.AfterRunningAdvice;

import java.lang.reflect.Method;

public class AfterMethod implements AfterRunningAdvice {

    @Override
    public Object after(Object returnVal, Method method, Object[] args, Object target) {
        System.out.println("后置通知增强功能......");
        return returnVal;
    }
}
