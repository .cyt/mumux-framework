package com.mumux.framework.aop.test;

import com.mumux.framework.aop.core.AopApplictionContext;
import com.mumux.framework.aop.core.XmlAopApplicationContext;

public class MainTest {

    public static void main(String[] args) throws Exception {

        AopApplictionContext aopApplictionContext = new AopApplictionContext("application.json");
//        XmlAopApplicationContext aopApplictionContext = new XmlAopApplicationContext("application.xml");
        aopApplictionContext.init();

        TestService testService = (TestService) aopApplictionContext.getBean("testServiceProxy");

        testService.testMethod();


    }
}
