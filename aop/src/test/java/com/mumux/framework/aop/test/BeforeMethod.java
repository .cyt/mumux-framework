package com.mumux.framework.aop.test;

import com.mumux.framework.aop.advisor.BeforeMethodAdvice;

import java.lang.reflect.Method;

public class BeforeMethod implements BeforeMethodAdvice{

    @Override
    public void before(Method method, Object[] args, Object target) {
        System.out.println("前置通知方法增强.......");
    }
}
