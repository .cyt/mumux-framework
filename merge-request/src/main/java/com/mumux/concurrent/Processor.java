package com.mumux.concurrent;

import java.util.List;

/**
 *@Author hjc
 *@Date 2019/2/14 1:54
 *@Description 面向接口编程，提升系统扩展性：
 */
public interface Processor<T> {

    void process(List<T> list);

}
